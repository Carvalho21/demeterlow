<?php

class Author
{
    public function __construct(
        public string $name,
        public int $age,
        public string $country
    ) {
    }
}
