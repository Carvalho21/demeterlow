<?php

class CreditCard
{
    public const STATUS_CONFIRMED = 'CONFIRMED';

    public function __construct(
        public string $creditNumber,
        public string $dueDate,
        public string $securyCode,
        public string $flag,
        public string $emissionDate,
        public string $status
    ) {
    }

    public function confirmedCard(): void
    {
        if ($this->status != CreditCard::STATUS_CONFIRMED) {
            throw new Exception('Invalid card. Please check your card and try again with a valid card.');
        }
    }
}
