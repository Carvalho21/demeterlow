<?php

class Payment
{
    public function __construct(
        private float $value,
        private int $installmentsNumber,
        private string $paymentType,
        private CreditCard $creditCard
    ) {
    }

    public function pay(): void
    {
        $this->creditCard->confirmedCard();

        echo "<<<< Payment realeased sucessfully. Thanks you for used our Payment System. >>>>";
    }
}
