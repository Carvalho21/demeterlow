<?php

class Item
{
    private const MAX_COUNT = 5;

    public function __construct(
        public int $count,
        public string $name,
        public float $value,
        public ?string $description
    ) {
        if ($count > self::MAX_COUNT) {
            throw new Exception('It\'s not possible to insert more than 5 items of the same kind.');
        }
    }
}
