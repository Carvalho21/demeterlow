<?php

class Stock
{
    public function __construct(
        public string $code,
        public float $currentPrice,
        public string $sector,
        public int $count
    ) {
    }

    public function validateIfEmptyCount(): void
    {
        if ($this->count === 0) {
            throw new Exception('It\'s not possible to update an empty stock.');
        }
    }
}
