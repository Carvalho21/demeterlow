<?php

class Story
{
    public array $stocks;

    public function updateStock(Stock $stock): void
    {
        $this->stock->validateIfEmptyCount();

        $this->stocks[] = $stock; 
    }
}