<?php

class Address
{
    public function __construct(
        private string $street,
        private string $city,
        private string $state,
        private int $number,
        private string $zipCode,
        private ?string $complement
    ) {
    }
}
