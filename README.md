Exercícios sobre a Lei de Demeter:
    
    1. Considere uma classe chamada Cliente que tem uma propriedade chamada endereco que contém informações sobre o endereço do cliente, como rua, cidade, estado, etc. Escreva um código que viola a Lei de Demeter nessa classe. Em seguida, modifique o código para seguir a Lei de Demeter.

    2. Suponha que você tenha uma classe chamada CarrinhoDeCompras que tenha um método chamado adicionarItem que recebe um objeto do tipo Item. Escreva um código que viole a Lei de Demeter nessa classe. Em seguida, modifique o código para seguir a Lei de Demeter.

    3. Considere uma classe chamada Livro que tenha uma propriedade chamada autor que contém informações sobre o autor do livro, como nome, idade, país, etc. Escreva um código que siga a Lei de Demeter nessa classe.

    4. Suponha que você tenha uma classe chamada Pagamento que tenha um método chamado efetuarPagamento que recebe um objeto do tipo CartaoDeCredito. Escreva um código que viole a Lei de Demeter nessa classe. Em seguida, modifique o código para seguir a Lei de Demeter.

    5. Considere uma classe chamada Loja que tenha uma propriedade chamada estoque que contém informações sobre o estoque de produtos da loja. Escreva um código que siga a Lei de Demeter nessa classe.

